from .app import db, login_manager


class Band(db.Model):
    __tablename__ = 'band'
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(100))

    def __repr__(self):
        return "<Band (%d) %s>" % (self.id, self.name)


class Album(db.Model):
    __tablename__ = 'album'
    id          = db.Column(db.Integer, primary_key=True)
    entryId     = db.Column(db.Integer)
    img         = db.Column(db.String(500))
    parent      = db.Column(db.String(200))
    releaseYear = db.Column(db.Integer)
    title       = db.Column(db.String(300))
    genres      = db.relationship("Association", back_populates="album")
    band_id     = db.Column(db.Integer, db.ForeignKey("band.id"))
    band        = db.relationship("Band", backref=db.backref("album", lazy="dynamic"))
    users_album = db.relationship("Favoris", back_populates="album")

    def __repr__(self):
        return "<Album (%d) %s>" % (self.id, self.title)

class Genre(db.Model):
    __tablename__ = 'genre'
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(200))
    albums       = db.relationship("Association", back_populates="genre")

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.name)

class Association(db.Model):
    __tablename__ = 'association'
    album_id    = db.Column(db.Integer, db.ForeignKey('album.id'), primary_key=True)
    genre_id    = db.Column(db.Integer, db.ForeignKey('genre.id'), primary_key=True)
    genre       = db.relationship("Genre",back_populates="albums")
    album       = db.relationship("Album", back_populates="genres")

    def __repr__(self):
        return "<Association (%d) %s>" % (self.album_id, self.genre_id)


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

from flask_login import UserMixin
class User(db.Model, UserMixin):
    __tablename__='user'
    username    = db.Column(db.String(50), primary_key=True)
    password    = db.Column(db.String(64))
    nom         = db.Column(db.String(72))
    prenom      = db.Column(db.String(72))
    email       = db.Column(db.String(120), nullable=False)
    albums      = db.relationship("Favoris", back_populates="user")

    def get_id(self):
        return self.username

class Favoris(db.Model):
    __tablename__='favoris'
    user_id     = db.Column(db.String(50), db.ForeignKey('user.username'), primary_key=True)
    album_id    = db.Column(db.Integer, db.ForeignKey('album.id'), primary_key=True)
    album       = db.relationship("Album", back_populates="users_album")
    user        = db.relationship("User", back_populates="albums")

    def __repr__(self):
        return "<Association (%s) %s>" % (self.user_id, self.album_id)

def get_band(id_band):
    return Band.query.get_or_404(id_band)

def get_album(id_album):
    return Album.query.get(id_album)

def get_genre(id_genre):
    return Genre.query.get(id_genre)

def get_album_home(NombreAlbum=10):
    return Album.query.limit(NombreAlbum).all()

def get_titre_album(id_album):
    return Album.query.get(id_album).title

def get_name_genre(id_genre):
    return Genre.query.get(id_genre).name

def get_album_with_title(titre_album):
    return Album.query.filter(Album.title==titre_album).all()[0].id

def get_genre_with_name(name_genre):
    return Genre.query.filter(Genre.name==name_genre).all()[0].id

def get_genre_with_ID_Album(id_album):
    return Association.query.filter(Association.album_id==id_album).all()

def get_tout_les_albums():
    return Album.query.order_by(Album.title)

def get_tout_les_albums_by_year():
    return Album.query.order_by(Album.releaseYear)

def get_tout_les_albums_by_year_desc():
    return Album.query.order_by(Album.releaseYear.desc())

def get_tout_les_band():
    return Band.query.order_by(Band.name)

def get_tout_les_genre():
    return Genre.query.order_by(Genre.name)

def get_album_with_artiste(id):
    return Album.query.filter(Album.band_id==id).order_by(Album.title)

def get_album_with_genre(id):
    return Association.query.filter(Association.genre_id==id).all()

def get_user_with_name(username):
    return User.query.filter(User.username==username).all()

def get_albums_favoris(username):
    return Favoris.query.filter(Favoris.user_id==username).all()

def get_album_precis_favoris(username, id):
    return Favoris.query.filter(Favoris.user_id==username, Favoris.album_id==id).all()

def sup_album(id):
    return Album.query.filter(Album.id==id).delete()

def sup_band(id):
    return Band.query.filter(Band.id==id).delete()

def sup_album_favoris(id, username):
    return Favoris.query.filter(Favoris.user_id==username, Favoris.album_id==id).delete()
