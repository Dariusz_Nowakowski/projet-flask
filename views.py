from .app import app, db
from flask import render_template, redirect, url_for,request
from .models import *
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, HiddenField, IntegerField, SelectField
from hashlib import sha256
from flask_login import login_user, current_user,login_required
from flask_wtf.file import FileField, FileAllowed

@app.route("/")
def home():
    return render_template("home.html", title="Spot-Hifi", albums=get_album_home())

@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/login')

@app.route("/album/<id_album>")
@login_required
def affiche_album(id_album):
    id_genre = get_genre_with_ID_Album(id_album)
    nom_genre=[]
    for id in id_genre:
        nom_genre.append(get_name_genre(id.genre_id))
    return render_template("affichage_album.html", title=get_titre_album(id_album),
     album=get_album(id_album), genres=nom_genre)


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


@app.route("/login/", methods=("GET","POST",))
def login():
    erreur = False
    f = LoginForm()
    if f.validate_on_submit():
        erreur = True
        user = f.get_authenticated_user()
        if user :
            erreur = False
            login_user(user)
            return redirect(url_for('home'))
    return render_template("login.html",form=f,title="Login", erreur = erreur)

from flask_login import logout_user
@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


class BandForm(FlaskForm):
    id      = HiddenField('id')
    name    = StringField('Name', validators=[DataRequired()])

@app.route("/edit/band/<int:id>")
@login_required
def edit_band(id):
		a = get_band(id)
		f = BandForm(id=a.id, name=a.name)
		return render_template(
			"edit-band.html",
			band=a, form=f, title="Édition")

@app.route("/save/band/", methods=("POST",))
@login_required
def save_band():
	a = None
	f = BandForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_band(id)
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for('home'))
	a = get_band(int(f.id.data))
	return render_template(
		"edit-band.html",
		band=a, form=f)

@app.route("/delete/band/<int:id>")
@login_required
def delete_band(id):
    sup_band(id)
    db.session.commit()
    return redirect(url_for('home'))


class AlbumForm(FlaskForm):
    id          = HiddenField('id')
    parent      = StringField('Name', validators=[DataRequired()])
    releaseYear = IntegerField('ReleaseYear', validators=[DataRequired()])
    title       = StringField('Title', validators=[DataRequired()])

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
    a = get_album(id)
    f = AlbumForm(id = a.id, parent=a.parent,
    releaseYear=a.releaseYear, title=a.title,
    # img=a.img
    )
    return render_template(
        "edit-album.html",
        album=a, form=f, title="Édition")

@app.route("/save/album/", methods=("POST",))
@login_required
def save_album():
    a = None
    f = AlbumForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_album(id)
        a.parent        = f.parent.data
        a.releaseYear   = f.releaseYear.data
        a.title         = f.title.data
        db.session.commit()
        return redirect(url_for('home'))
    a = get_band(int(f.data.id))
    return render_template(
    "edit-album.html", author=a, form=f)

@app.route("/delete/album/<int:id>/<string:username>")
@login_required
def delete_album(id, username):
    sup_album_favoris(id, username)
    sup_album(id)
    db.session.commit()
    return redirect(url_for('home'))

@app.route("/ajout_favoris/<int:id>/<string:username>")
@login_required
def ajout_favoris(id, username):
    erreur = False
    if(get_album_precis_favoris(username,id)):
        erreur=True
        return redirect(url_for('affiche_album',id_album=id))
    else :
        fav = Favoris(
            user_id     = username,
            album_id    = id
        )
        db.session.add(fav)
        db.session.commit()
        return redirect(url_for('home'))

class InscriptionForm(FlaskForm):
        username    = StringField('Username')
        prenom      = StringField('First name')
        nom         = StringField('Last name')
        email       = StringField('Email')
        password    = PasswordField('Password')
        confirmePsw = PasswordField('Confirm Password')

        def valideUsername(self):
            user = User.query.get(self.username.data)
            if user is None:
                print(self.confirmePsw.data)
                return self if self.confirmePsw.data == self.password.data else 2
            else :
                return 1;

@app.route("/inscription/", methods=("GET","POST",))
def inscription():
    erreur = 0
    f = InscriptionForm()
    if f.validate_on_submit():
        user = f.valideUsername()
        if user!=1 and user!=2 and user :
            m = sha256()
            m.update(user.password.data.encode())
            u = User(username=user.username.data, prenom=user.prenom.data,
            nom=user.nom.data, email=user.email.data, password=m.hexdigest())
            db.session.add(u)
            db.session.commit()
            login_user(u)
            return redirect(url_for('home'))
        elif(user == 1):
            erreur = 1

        else :
            erreur = 2
    return render_template("inscription.html",form=f, erreur=erreur, title="Inscription")



@app.route("/nos_artistes/")
@login_required
def catalogue_band():
    return render_template("catalogue_band.html", band=get_tout_les_band(), title="Artiste")

@app.route("/nos_artistes/<int:id>")
@login_required
def catalogue_band_album(id):
    return render_template("catalogue_band_album.html", albums=get_album_with_artiste(id), artiste=get_band(id))



@app.route("/nos_genres/")
@login_required
def catalogue_genre():
    return render_template("catalogue_genre.html", genre=get_tout_les_genre(), title="Genres")

@app.route("/nos_genres/<int:id>")
@login_required
def catalogue_genre_album(id):
    id_album = get_album_with_genre(id)
    album=[]
    for a in id_album:
        album.append(get_album(a.album_id))
    return render_template("catalogue_genre_album.html", albums=album, genre=get_genre(id), title=get_genre(id))


@app.route("/profil/<string:username>")
@login_required
def profil(username):
    albums_favoris=get_albums_favoris(username)
    album_liste=[]
    for id in albums_favoris:
        album_liste.append(get_album(id.album_id))
    user=get_user_with_name(username)
    return render_template("profil.html",user=get_user_with_name(username), title="Profil", albums=album_liste)


class CreationAlbumForm(FlaskForm):
    id          = HiddenField('id')
    entryId     = IntegerField('Le nombre d\'entré')
    parent      = StringField('Son auteur', validators=[DataRequired()])
    releaseYear = IntegerField('Date de sortie', validators=[DataRequired()])
    title       = StringField('Titre de l\'album', validators=[DataRequired()])
    audio       = StringField('Lien d\'écoute Spotify')
    img         = FileField('Image de l\'album',)

@app.route("/ajout/album", methods=("GET", "POST"))
@login_required
def ajout_album():
    erreur = 0
    f = CreationAlbumForm()
    if f.validate_on_submit():
        title = f.title.data
        a = Album(entryId=f.entryId.data,
                  img=f.img.data,
                  parent=f.parent.data,
                  releaseYear=f.releaseYear.data,
                  title=f.title.data,
                  audio=f.audio.data,
                  )
        print("coucou")
        db.session.add(a)
        db.session.commit()
        return redirect(url_for("choisir"))
    return render_template("creationAlbum.html", form=f, erreur=erreur, title="Création Album")
