import click
from .app import app, db
from .models import *

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    """
        creates the tables and populates them with data.
    """
    db.create_all()

    import yaml
    albums = yaml.load(open(filename))

    from .models import Band, Genre, Album

    band = {}
    for b in albums:
        ba=b["by"]
        if ba not in band:
            o = Band(name=ba)
            db.session.add(o)
            band[ba] = o
    db.session.commit()

    genre=dict()
    for g in albums:
        i = 0
        ge=g["genre"]
        while i < len(ge):
            if ge[i] not in genre:
                o = Genre(name=ge[i])
                db.session.add(o)
                genre[ge[i]] = o
            i +=1
    o = Genre(name="aucun")
    genre["noGenre"]=o
    db.session.commit()

    album_dico =dict()
    for a in albums:
        al = band[a["by"]]
        o = Album(entryId       = a["entryId"],
                  img           = a["img"],
                  parent        = a["parent"]  ,
                  releaseYear   = a['releaseYear'],
                  title         = a['title'],
                  band_id       = al.id
                  )
        db.session.add(o)
        album_dico[a["by"]]=o
    db.session.commit()

    for a in albums:
        i = 0
        while i < len(a["genre"]) :
            id_Al = get_album_with_title(a["title"])
            id_Ge = get_genre_with_name(a["genre"][i])
            o = Association(
            album_id     = id_Al,
            genre_id    = id_Ge
            )
            db.session.add(o)
            i+=1
    db.session.commit()

@app.cli.command()
def syncdb():
    """
        Creates all missing tables.
    """
    db.create_all()

@app.cli.command()
def deldb():
    """
        Drop all tables in your DataBases
    """
    db.drop_all()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    """
        Adds a new user
    """
    from.models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username = username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()
